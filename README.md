### 学习 netty
1. netty三要素
    - server
        - 服务器的声明
    - handle
        - handle的配置
    - initializer
        - 定义管道
        
### gradle的基本使用(gradle version 4.4.1)
1. 清空
    ```
    gradle clean build
    ```
2. 构建
    ```
    gradle build
    ```
3. wrapper
    ```
    ./gradlew build
    ```
4. gradlew(gradle wrapper)的好处。
    - 它的作用是使用者在不安装gradle的情况下，依然可以通过一条很简单的命令就可以实现项目的构建
    -  通过gradle wrapper下载的gradle和你自己下载的gradle是不一样的，通过gradlew下载的gradle默认安装在
    `/Users/youshang/.gradle/wrapper/dists` 下，可以简单理解为只为gradlew提供的gradle支持
    - 方便开发者构建代码，并且可以统一gradle版本，推荐使用gradlew操作
    - 使用gradlew 需要关注一下 项目中的gradle/wrapper/gradle-wrapper.properties文件
：
#### 报错信息
1. 在使用gradle 7.x的时候出现的报错
    ```
    FAILURE: Build failed with an exception.
    
    * Where:
    Build file '/Users/youshang/IdeaProjects/private/netty-learning/build.gradle' line: 24
    
    * What went wrong:
    A problem occurred evaluating root project 'nettp-learning'.
    > Cannot add task 'wrapper' as a task with that name already exists.
    
    * Try:
    Run with --stacktrace option to get the stack trace. Run with --info or --debug option to get more log output. Run with --scan to get full insights.
    
    * Get more help at https://help.gradle.org
    
    BUILD FAILED in 0s
    ```
   解决办法，降版本降到7.x以下就可以解决

### protocol buffer的基本使用
1. 下载 protocol buffer编译器
[官网下载](https://github.com/protocolbuffers/protobuf/releases) (max/window)需要下载 `protoc-3.17.3-osx-x86_64.zip`
    - 安装好了之后推荐配置进环境变量中，方便之后直接使用`protoc`
2. 编写protocol buffer中的massage
    ```
   syntax = "proto2"; //表示使用的proto2
     
     package tutorial; //声明包名(对于java之外的声明)
     
     option java_multiple_files = true; //是否生成多个Java文件
     option java_package = "com.example.tutorial.protos"; //生成后的文件存放在那个包下
     option java_outer_classname = "AddressBookProtos"; //生成的Java文件在外部的类名
     
     message Person { // 对应一个Java类
       optional string name = 1; //声明一个可选的 string类型的 name字段，并且标记为1 (1~15) (问题1：在变量后面声明数字)   
       optional int32 id = 2; //int32 对应Java中的int
       required string email = 3;  //声明一个必定会存在的email，当这个属性没被初始化的时候会报错(推荐使用optional)
     
       enum PhoneType { //声明一个枚举
         MOBILE = 0;
         HOME = 1;
         WORK = 2;
       }
     
       message PhoneNumber { //在parson中声明一个内部对象
         optional string number = 1;
         optional PhoneType type = 2 [default = HOME]; //当前枚举设置一个默认值为 HOME
       }
     
       repeated PhoneNumber phones = 4;
     }
     
     message AddressBook {
       repeated Person people = 1; //一个复数的存在，该字段可以重复任意次数
     }
   问题1
   解答：
   每个元素上的"= 1"、"= 2" 标记标识字段在二进制编码中使用的唯一“标签”。
   标记数字 1-15 需要比更高数字少一个字节来编码，因此作为一种优化，
   您可以决定将这些标记用于常用或重复的元素，而将标记 16 和更高的标记用于不太常用的可选元素。
   重复字段中的每个元素都需要重新编码标签编号，因此重复字段特别适合这种优化。(直接在官网上面抄的，建议去官网查看)
     ```
3. 执行编写好的Person.proto文件    
    ```
    protoc -h (相当于help)
    执行编译 protoc --java_out=src/main/java src/protobuf/Person.proto (重复执行会覆盖之前的)
    ```
4. 针对生成protocol buffer基本的操作
    ```
    请看com.youshang.protobuf_1包下的全部example
    ```
------
### apache thrift的基本使用 
1. 安装apache thrift [官网地址](https://thrift.apache.org/)
    - mac和Linux推荐使用(https://brew.sh/) 使用home brew工具安装，这个包管理工具下载比较方便，只需要执行 `brew install thrift`就好了(前提需要配置git)
2. 定义struct
    ```
    详细请看：src/thrift/Person.thrift
    ```
3. 编译.thrift文件
    ```
    thrift --gen java src/thrift/Person.thrift
    ```
4. 操作.thrift文件
    ```
    请看这个包下的内容 com.youshang.thrift
    ```
### google grpc的基本使用 


####grpc插件使用
1. 注意使用插件的gradle版本，在gradle.5.x版本之后不支持自动生成文件

2. 报错信息：
![avatar](src/main/image/gradle-build-grpc-version-error.jpg)
- 解决办法： 提升grpc的构建脚本依赖的版本升级，从原先的0.8.0 提升到 0.8.4


###grpc和thrift的区别
1. 在接受参数的时候
- grpc只能接受message类型
- thrift可以接收的参数不受限制