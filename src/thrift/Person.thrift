namespace java com.youshang.thrift.generated

typedef i16 Short //将thrift中的类型取一个别名的意思，增加可读性
typedef i32 int
typedef string String
typedef bool boolean

struct Person{ //相对于Java来说 等于创建一个实体对象,其中的1: 的作用和protocol buffer是一样的
    1: optional String username,
    2: optional int age,
    3: optional boolean flag,
    4: optional Short score;
}
exception DataException{ //定义一个异常
    1: optional String code;
    2: optional String message;
    3: optional String data;
}
service PersonService{ //定义一个service
    Person getPersonByUserName(1: required String userName) throws (1: DataException dataException);

    void savePerson(1: required Person person) throws (1: DataException dataException);
}