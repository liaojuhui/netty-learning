package com.youshang.grpc.client;

import com.google.common.base.Throwables;
import com.youshang.proto.*;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;

import java.util.Iterator;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * @author youshang
 * @date 2021/08/29 13:14
 **/
public class GrpcClient {


    public static void main(String[] args) throws InterruptedException {
        ManagedChannel managedChannel = ManagedChannelBuilder.forAddress("localhost", 8899).usePlaintext().build();

        System.out.println("============当请求参数和返回参数都是普通 message类型时==================");
        StudentServiceGrpc.StudentServiceBlockingStub studentServiceBlockingStub = StudentServiceGrpc.newBlockingStub(managedChannel);
        MyResponse zs = studentServiceBlockingStub.getRealNameByUsername(MyRequest.newBuilder().setUserName("李四").build());
        System.out.println("zs.getRealName() = " + zs.getRealName());


        /*System.out.println("============当前返回参数是 stream 时==================");


        Iterator<StudentResponse> studentByAges = studentServiceBlockingStub.getStudentByAges(StudentRequest.newBuilder().setAge(15).build());
        while (studentByAges.hasNext()) {
            StudentResponse next = studentByAges.next();
            System.out.println(next.getAge() + "  " + next.getName());
        }

        System.out.println("============当前请求参数是 stream 时==================");
        StudentServiceGrpc.StudentServiceStub serviceStub = StudentServiceGrpc.newStub(managedChannel);

        StreamObserver<StudentResponseList> studentResponseListStreamObserver = new StreamObserver<StudentResponseList>() {
            @Override
            public void onNext(StudentResponseList value) {
                for (StudentResponse studentResponse : value.getStudentResponseList()) {
                    System.out.println("studentResponse.getName() = " + studentResponse.getName());
                    System.out.println("studentResponse.getAge() = " + studentResponse.getAge());
                }
            }

            @Override
            public void onError(Throwable t) {
                System.out.println("客户端异常信息 : " + Throwables.getStackTraceAsString(t));
            }

            @Override
            public void onCompleted() {

            }
        };

        StreamObserver<StudentRequest> studentsWrapperByAges = serviceStub.getStudentsWrapperByAges(studentResponseListStreamObserver);
        studentsWrapperByAges.onNext(StudentRequest.newBuilder().setAge(22).build());
        studentsWrapperByAges.onNext(StudentRequest.newBuilder().setAge(23).build());
        studentsWrapperByAges.onCompleted();
        Thread.sleep(10000); //为了看到结果实现了休眠


        System.out.println("============当前请求参数和返回参数都是stream 时==================");
        StreamObserver<StudentResponseList> responseListStreamObserver = new StreamObserver<StudentResponseList>() {
            @Override
            public void onNext(StudentResponseList value) {
                value.getStudentResponseList().forEach(response -> {
                    int age = response.getAge();
                    String name = response.getName();
                    System.out.println("name = "+ name+ "  ,age =" +age );
                });
            }

            @Override
            public void onError(Throwable t) {
                System.out.println("报错异常: "+ Throwables.getStackTraceAsString(t));
            }

            @Override
            public void onCompleted() {
                System.out.println("onCompleted.......");
            }
        };
        StudentResponseList.Builder studentResponseList = StudentResponseList.newBuilder();
        for (int i = 0; i < 10; i++) {
            studentResponseList.addStudentResponse(StudentResponse.newBuilder().setName("李四"+i).setAge(40+i).build());
        }
        responseListStreamObserver.onNext(studentResponseList.build());
        responseListStreamObserver.onCompleted();
        Thread.sleep(5000);


        StreamObserver<StudentRequestList> studentRequestListStreamObserver = serviceStub.biTalk(responseListStreamObserver);

        StudentRequestList.Builder studentRequestList = StudentRequestList.newBuilder();
        for (int i = 0; i < 10; i++) {
            studentRequestList.addStudentRequest(StudentRequest.newBuilder().setAge(20+i).build());
        }
        studentRequestListStreamObserver.onNext(studentRequestList.build());
        studentRequestListStreamObserver.onCompleted();
        Thread.sleep(5000);//为了看到结果实现了休眠*/


    }
}
