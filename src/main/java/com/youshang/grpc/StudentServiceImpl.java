package com.youshang.grpc;

import com.google.common.base.Throwables;
import com.youshang.proto.*;
import io.grpc.stub.StreamObserver;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.IntStream;

/**
 *
 * @author youshang
 * @date 2021/08/26 22:32
 **/
public class StudentServiceImpl extends StudentServiceGrpc.StudentServiceImplBase {
    @Override
    public void getRealNameByUsername(MyRequest request, StreamObserver<MyResponse> responseObserver) {
        System.out.println("服务端接受到的消息: "+request.getUserName());

        responseObserver.onNext(MyResponse.newBuilder().setRealName("张三").build());
        responseObserver.onCompleted();
    }

    @Override
    public void getStudentByAges(StudentRequest request, StreamObserver<StudentResponse> responseObserver) {
        System.out.println("服务端接收到的消息: "+ request.getAge());

        responseObserver.onNext(StudentResponse.newBuilder().setName("张三").setAge(22).build());
        responseObserver.onNext(StudentResponse.newBuilder().setName("李四").setAge(24).build());
        responseObserver.onNext(StudentResponse.newBuilder().setName("王五").setAge(25).build());
        responseObserver.onNext(StudentResponse.newBuilder().setName("赵六").setAge(26).build());
        responseObserver.onCompleted();
    }

    @Override
    public StreamObserver<StudentRequest> getStudentsWrapperByAges(StreamObserver<StudentResponseList> responseObserver) {
        return new StreamObserver<StudentRequest>() {

            @Override
            public void onNext(StudentRequest value) {
                System.out.println("请求参数：" + value.getAge());
            }

            @Override
            public void onError(Throwable t) {
                System.out.println("请求异常: "+ Throwables.getStackTraceAsString(t));
            }

            @Override
            public void onCompleted() {
                StudentResponse response = StudentResponse.newBuilder().setName("张三").setAge(22).build();
                StudentResponse response2 = StudentResponse.newBuilder().setName("李四").setAge(23).build();
                StudentResponseList responseList = StudentResponseList.newBuilder().addStudentResponse(response).addStudentResponse(response2).build();
                responseObserver.onNext(responseList);
                responseObserver.onCompleted();
            }
        };
    }

    @Override
    public StreamObserver<StudentRequestList> biTalk(StreamObserver<StudentResponseList> responseObserver) {
        return new StreamObserver<StudentRequestList>() {

            @Override
            public void onNext(StudentRequestList value) {
                List<StudentRequest> studentRequestList = value.getStudentRequestList();
                studentRequestList.forEach(request -> {
                    System.out.println("request.getAge() : "+request.getAge());
                });
            }

            @Override
            public void onError(Throwable t) {
                System.out.println("请求异常： "+ Throwables.getStackTraceAsString(t));
            }

            @Override
            public void onCompleted() {

                StudentResponseList.Builder builder = StudentResponseList.newBuilder();
                for (int i = 0; i < 10; i++) {
                    builder.addStudentResponse(StudentResponse.newBuilder().setAge(30+i).setName("张三"+i).build());
                }

                responseObserver.onNext(builder.build());
                responseObserver.onCompleted();

            }
        };
    }
}
