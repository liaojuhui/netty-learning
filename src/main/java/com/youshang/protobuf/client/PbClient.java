package com.youshang.protobuf.client;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

/**
 *
 * @author youshang
 * @date 2021/08/12 22:06
 **/
public class PbClient {

    public static void main(String[] args) throws InterruptedException {
        NioEventLoopGroup eventExecutors = new NioEventLoopGroup();
        try {
            Bootstrap bootstrap = new Bootstrap();
            bootstrap.group(eventExecutors).channel(NioSocketChannel.class).handler(new PbClientInitializer());
            bootstrap.connect("localhost",8899).sync();
        }finally {
            //eventExecutors.shutdownGracefully();
        }
    }
}
