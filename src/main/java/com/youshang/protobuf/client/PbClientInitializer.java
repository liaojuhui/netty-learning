package com.youshang.protobuf.client;

import com.youshang.protobuf.common.Message;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.protobuf.ProtobufDecoder;
import io.netty.handler.codec.protobuf.ProtobufEncoder;
import io.netty.handler.codec.protobuf.ProtobufVarint32FrameDecoder;
import io.netty.handler.codec.protobuf.ProtobufVarint32LengthFieldPrepender;

/**
 *
 * @author youshang
 * @date 2021/08/12 22:07
 **/
public class PbClientInitializer extends ChannelInitializer<SocketChannel> {
    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline pipeline = ch.pipeline();
        /*pipeline.addLast("my-client-ProtobufVarint32FrameDecoder",new ProtobufVarint32FrameDecoder());//半包处理
        pipeline.addLast("my-client-ProtobufDecoder",new ProtobufDecoder(MyStudent.Student.getDefaultInstance()));//只负责解码，不负责拆包粘包处理，所以在这之前要添加ProtobufVarint32FrameDecoder
        pipeline.addLast("my-client-ProtobufVarint32LengthFieldPrepender",new ProtobufVarint32LengthFieldPrepender());
        pipeline.addLast("my-client-ProtobufEncoder",new ProtobufEncoder());//PB协议编码器*/

        pipeline.addLast(new ProtobufVarint32FrameDecoder());//添加protobuff解码器
        pipeline.addLast(new ProtobufDecoder(Message.MessageBase.getDefaultInstance()));//添加protobuff对应类解码器
        pipeline.addLast(new ProtobufVarint32LengthFieldPrepender());//protobuf的编码器 和上面对对应
        pipeline.addLast(new ProtobufEncoder());//protobuf的编码器

        pipeline.addLast("my-client-PbClientHandle",new PbClientHandle());
    }
}
