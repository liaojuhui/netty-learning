package com.youshang.zerocopy;

import org.apache.catalina.Server;

import java.io.DataInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.channels.ServerSocketChannel;

/**
 *  原始的数据传输方式 - 服务端
 * @author youshang
 * @date 2021/11/25 22:17
 **/
public class OldIOServer {

    public static void main(String[] args) throws Exception {
        ServerSocket serverSocket = new ServerSocket(8899);
        while (true){
            Socket socket = serverSocket.accept();
            DataInputStream inputStream = new DataInputStream(socket.getInputStream());

            byte[] readByte = new byte[4096];
            while (true){
                int read = inputStream.read(readByte, 0, readByte.length);
                if (read == -1){
                    break;
                }
            }
        }
    }
}
