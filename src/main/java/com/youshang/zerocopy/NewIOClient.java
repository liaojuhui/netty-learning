package com.youshang.zerocopy;

import java.io.FileInputStream;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

/**
 * 使用netty零拷贝 操作数据-客户端
 * @author youshang
 * @date 2021/11/25 22:59
 **/
public class NewIOClient {
    public static void main(String[] args) throws Exception{
        long startTime = System.currentTimeMillis();
        SocketChannel socketChannel = SocketChannel.open();
        socketChannel.connect(new InetSocketAddress("localhost",8899));

        String filePath = "/Users/youshang/Downloads/confluent-community-6.0.0.tar";
        FileInputStream inputStream = new FileInputStream(filePath);
        FileChannel channel = inputStream.getChannel();
        inputStream.getChannel().transferTo(0,channel.size(),socketChannel);

        System.out.println("发送成功, 消耗时间:"+ (System.currentTimeMillis() - startTime));//130
        inputStream.close();
        socketChannel.close();
    }
}
