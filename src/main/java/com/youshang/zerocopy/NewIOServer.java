package com.youshang.zerocopy;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

/**
 * 使用netty零拷贝 操作数据-服务端
 * @author youshang
 * @date 2021/11/25 22:49
 **/
public class NewIOServer {

    public static void main(String[] args) throws Exception {
        ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
        InetSocketAddress address = new InetSocketAddress(8899);
        serverSocketChannel.bind(address);

        while (true){
            SocketChannel socketChannel = serverSocketChannel.accept();
            ByteBuffer readByteBuffer = ByteBuffer.allocate(4096);

            while (socketChannel.read(readByteBuffer) != -1){
                readByteBuffer.rewind();
            }
        }

    }
}
