package com.youshang.zerocopy;

import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.net.Socket;

/**
 * 原始的数据传输方式 - 客户端
 * @author youshang
 * @date 2021/11/25 22:32
 **/
public class OldIOClient {
    public static void main(String[] args) throws Exception {
        long startTime = System.currentTimeMillis();
        Socket socket = new Socket("localhost",8899);

        String filePath = "/Users/youshang/Downloads/confluent-community-6.0.0.tar";
        FileInputStream inputStream = new FileInputStream(filePath);

        DataOutputStream outputStream = new DataOutputStream(socket.getOutputStream());

        byte[] writeByte = new byte[4096];

        while (inputStream.read(writeByte,0,writeByte.length) >= 0){
            outputStream.write(writeByte);
        }
        System.out.println("发送完成， 消耗时间为:"+ (System.currentTimeMillis() - startTime));//330

        inputStream.close();
        outputStream.close();
        socket.close();

    }

}
