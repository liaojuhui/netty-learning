package com.youshang.decorator;
/**
 * 这个真实的目标实现
 * @author youshang
 * @date 2021/09/01 00:20
 **/
public class ConcreteComponent implements Component {
    @Override
    public void doSomething() {
        System.out.println("功能A");
    }
}
