package com.youshang.decorator;
/**
 *  TODO 核心所在
 *  装饰角色
 *  1. 实现抽象角色
 *  2. 持有一个构件(Component)对象的引用***，并定一个与抽象构件接口一致的接口
 * @author youshang
 * @date 2021/09/01 00:22
 **/
public class Decorator implements Component {

    private Component component;

    public Decorator(Component component){
        this.component = component;
    }

    @Override
    public void doSomething() {
        component.doSomething();
    }
}
