package com.youshang.decorator;
/**
 *  装饰模式的角色： 抽象构件角色，给出一个抽象接口，以规范准备接收附加责任的对象
 *  可以理解为InputStream
 * @author youshang
 * @date 2021/09/01 00:20
 **/
public interface Component {

    void doSomething();

}
