package com.youshang.decorator;
/**
 *
 * @author youshang
 * @date 2021/09/01 00:20
 **/
public class Client {

    public static void main(String[] args) {
        /**
         *
         * 1. Java的I/O库提供了一个称作链接的机制，可以将一个流与另一个流首尾相接，形成一个流管道的链接。这种机制实际上是一种被称为***Decorator(装饰)设计模式***的引用
         * 2. 通过流的链接，可以动态的增加流的功能，而这种功能的增加是通过组合一些流的基本功能而动态获取的
         * 3. 我们要获取一个I/O对象，往往需要产生多个I/O对象，这也是Java I/O库不太容易掌握的原因，但在I/O库中Decorator模式的运用。给我们提供了实现上的灵活性。
         *
         * 经过调用发现，在原本的Component方法中，在执行原有"功能A" 的情况下，新增了"功能C"，"功能B"
         *
         */
        Component component = new ConcreteDecoratorB(new ConcreteDecoratorC(new ConcreteComponent()));
        component.doSomething();
    }
}