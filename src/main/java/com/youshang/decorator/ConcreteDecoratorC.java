package com.youshang.decorator;

/**
 * 装饰模式角色： 具体装饰角色(Concrete Decorator) ， 负责给构件对象 "贴上"附加的责任
 * @author youshang
 * @date 2021/09/01 00:21
 **/
public class ConcreteDecoratorC extends Decorator {
    public ConcreteDecoratorC(Component component) {
        super(component);
    }

    @Override
    public void doSomething() {
        this.doAnotherThing();
        super.doSomething();
    }
    private void doAnotherThing(){
        System.out.println("功能C");
    }
}
