package com.youshang.nio;

import java.nio.ByteBuffer;

/**
 * Buffer put 是什么类型 在get 的时候就应该是什么类型
 * 比如 buffer.putInt  在 get 的时候就应该用 getInt 否则会报错 : java.nio.BufferUnderflowException
 * @author youshang
 * @date 2021/09/02 00:30
 **/
public class Test5 {

    public static void main(String[] args) {
        ByteBuffer buffer = ByteBuffer.allocate(512);

        buffer.putInt(123);
        buffer.putLong(1234561123L);

        buffer.flip();

        System.out.println("buffer.getInt() = " + buffer.getInt());
        System.out.println("buffer.getLong() = " + buffer.getLong());
    }
}
