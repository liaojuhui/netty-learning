package com.youshang.nio;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.channels.FileChannel;
import java.security.SecureRandom;

/**
 * 学习Buffer , FileOutputStream 转换成 buffer ，反向将数据写入到 文件中
 * @author youshang
 * @date 2021/09/01 01:32
 **/
public class Test3 {

    public static void main(String[] args) throws Exception{
        FileOutputStream outputStream = new FileOutputStream("Test3.txt");
        FileChannel channel = outputStream.getChannel();

        ByteBuffer buffer = ByteBuffer.allocate(512);
        byte[] str = "youshang520i".getBytes();
        buffer.put(str);

        buffer.flip();

        channel.write(buffer);

    }
}
