package com.youshang.nio;

import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.channels.FileChannel;
import java.security.SecureRandom;

/**
 * 学习Buffer 通过FileInputStream 获取到buffer， 反向写入文件
 * @author youshang
 * @date 2021/09/01 01:32
 **/
public class Test2 {

    public static void main(String[] args) throws  Exception{
        FileInputStream fileInputStream = new FileInputStream("Test2.txt");
        FileChannel channel = fileInputStream.getChannel();

        ByteBuffer buffer = ByteBuffer.allocate(512);
        channel.read(buffer);

        buffer.flip();

        while (buffer.remaining() > 0){
            System.out.println("获取到的文件数据:"+ (char)buffer.get());
        }
        fileInputStream.close();

    }
}
