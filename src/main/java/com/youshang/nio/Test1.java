package com.youshang.nio;

import java.nio.IntBuffer;
import java.security.SecureRandom;

/**
 * 学习Buffer
 * @author youshang
 * @date 2021/09/01 01:32
 **/
public class Test1 {

    public static void main(String[] args) {
        IntBuffer buffer = IntBuffer.allocate(10);

        for (int i = 0; i < buffer.capacity(); i++) {
            int secureRandom = new SecureRandom().nextInt(20);
            buffer.put(secureRandom);
        }

        //当想要对象buffer进行 读写操作时一定要调用buffer的 buffer.flip() 方法
        buffer.flip();

        while (buffer.hasRemaining()){
            System.out.println(buffer.get());
        }

    }
}
