package com.youshang.nio;

import java.io.FileNotFoundException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;

/**
 * 字符集的编解码
 * @author youshang
 * @date 2021/09/23 23:58
 **/
public class Test9 {

    public static void main(String[] args) throws Exception {
        RandomAccessFile randomAccessInputFile = new RandomAccessFile("test9_in.txt","r");
        RandomAccessFile randomAccessOutputFile = new RandomAccessFile("test9_out.txt","rw");

        FileChannel inputFileChannel = randomAccessInputFile.getChannel();
        FileChannel outputFileChannel = randomAccessOutputFile.getChannel();

        long size = inputFileChannel.size();

        MappedByteBuffer mappedByteBuffer = inputFileChannel.map(FileChannel.MapMode.READ_ONLY, 0, size);

        Charset charset = Charset.forName("ISO-8859-1");
        CharsetDecoder decoder = charset.newDecoder();
        CharsetEncoder encoder = charset.newEncoder();

        CharBuffer charBuffer = decoder.decode(mappedByteBuffer);
        ByteBuffer byteBuffer = encoder.encode(charBuffer);

        outputFileChannel.write(byteBuffer);
        //1. 为什么使用iso-8859-1编码格式 还是可以正常输出中文，没有任何的乱码情况
        //答：
        // 1.编码和解码使用的是同一种编码格式
        // 2.使用ISO-8859-1对文件进行编码，使用UTF-8去查看文件，不会乱码，因为在编码的时候并没有对原有的字节进行任何的改动
        // 就算ISO-8859-1 和 UTF-8 对文件处理的编码方式不一样，但是字节始终没有发生变化，解码前是N个字节，编码后也是N个字节
        // 对于UTF-8来说每影响，只需要按照BOM(Byte Order Mark)去匹配就好了(一定要确保编码于解码时 使用的编码方式一样的)

    }
}
