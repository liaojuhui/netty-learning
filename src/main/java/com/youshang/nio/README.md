### 学习 nio
1. 学习nio 中要的三大组建
 - selector
 - channel
 - buffer

####  java.io  VS  java.noi
1. java.io 中最核心的是流(Stream)，面向流的编程。Java中，一个流只能是输入流/输出流，不可能同时是输入流又是输出流
2. java.nio 中拥有3个核心的概念(selector，Channel，Buffer)，在Java.nio中，是面向块(block)或是缓冲区(buffer)编程的，
   Buffer本身就是一块内存，底层实现上，他实际是一个数组，数据的读/写都是通过buffer实现的
- 除了数组外,Buffer还提供了对于数据的结构化访问方式，并且可以追踪到系统的读写过程。
- Java中的7种原生基本数据类型都有各自对于的buffer类型，如IntBuffer...LongBuffer等等.。(没有BooleanBuffer)
- Channel指的是可以向其写入数据或是从中读取数据的对象，它类似于Java中的Stream 与 Stream不同的是，
  Channel是双向的而Stream是单向的(InputStream/outputStream)
- 所有数据的读写都是通过Buffer来进行的。永远不会出现直接从Channel 读取或者写入数据的情况
- 由于Channel是双向的，因此它能更好的反映出底层操作系统的真实情况，在Linux系统中，底层操作系统的通道就是双向的


#### 0901留下的一个疑问
1. buffer.flip() 这个方法的作用是什么？中的每个参数起到什么作用？在读的时候起到什么作用？在写的时候起到什么作用？
答：
-   capacity 只要设置值就永远不会变化，同时也不可能为负数
-   limit 永远不会为负数，永远不会大于capacity，无法再去读/写 的第一个元素的索引
-   position 指的是下一个被读/写的元素的索引，不可能为负数
-   marking and resetting 标记和重制， 比如读取一个长度为10，此时读到5的时候marking一下
    当读到8的时候resetting一下，又会回到5的位置
-   0 <= marking <= position <= limit <= capacity

#### 学习buffer中常用方法的原理
1. 在buffer中clear的原理
答： 
- 将position设置为0，limit设置为capacity，capacity不变，数据由下一次调用的时候自动覆盖(并没有清空集合的动作)

 2.在buffer中调用flip的原理
答：
- 将position设置为0，position 赋值给 limit，实现读写反转

3.在buffer中使用allocate的原理
答：
- 将传入的参数赋值给capacity，limit position设置为0，mark设置为-1，offerSet设置为0

4. directByteBuffer 是存在与堆上的一个标准对象，但是它持有一个在操作系统上的一个 
directByteBuffer 直接内存模型 ->  操作系统
HeapByteBuffer  -> native -> 操作系统

###学习字符集
1. 最开始的时候诞生
- ASCII (American Standard Code for Information Interchange ,美国信息交换标准代码)
- ASCII最早的时候是为了满足纯英文的时候一种编码方法
- 7 bit表示一个字节
- 一个字节 = 8 bit，此时每使用ASCII会浪费一点点空间

2.随着计算机引用更加广泛，ASCII逐渐满足不了市场
- ISO-8859-1 诞生了
- ISO-8859-1 完全兼容了ASCII，并且在原先ASCII上进行了改进
- 使用 8 bit表示一个字节

3.中国的加入诞生了新的编码方式
- GB2312 早期的时候用作中文的编码方式
- 可以编码的中文有限

4.随着中文的博大精深逐渐不能满足中文的需求
- GBK诞生了
- GBK绝大部分的中文编码方式

5.现在中文支持最广泛的编码方式
- GBK18030

6.随着全世界的加入每个国家都制定了自己的编码方式为了统一
- unicode诞生了
- 采用两个字节来表示一个**字符**
- 统一了编码方式，随之而来的问题是存储问题
- 原先英文可以字节使用1个字节就能解决的问题，现在使用unicode也得使用两个字节

7.为了解决unicode的存储问题
- UTF (Unicode Translation Format) 诞生了
- 为了解决存储问题

8.UTF中的代表
- 根据计算机的存储架构，定制了一种规范
- UTF-16LE (little endian)
     - 文件头标志：0xFFFE
- UTF-16BE (big endian)
    - 文件头标志：0xFEFE
- 在Mac OS，Linux 中不存在，大部分情况下存在于window中
- 使用2个字节来表示一个字符

9.为了解决UTF-16文件存储
- UTF-8 诞生了
- 变长字节表示形式
- 对于英文是一个字节存储
- 对于中文是三个字节存储