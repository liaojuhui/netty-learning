package com.youshang.thirdExample.client;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.Delimiters;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.util.CharsetUtil;

/**
 *
 * @author youshang
 * @date 2021/08/08 22:05
 **/
public class MyChatClientInitializer extends ChannelInitializer<SocketChannel> {
    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline pipeline = ch.pipeline();
        pipeline.addLast("chat-client-delimiterBasedFrameDecoder",new DelimiterBasedFrameDecoder(4096, Delimiters.lineDelimiter()));
        pipeline.addLast("chat-client-stringDecoder",new StringDecoder(CharsetUtil.UTF_8));
        pipeline.addLast("chat-client-stringEncoder",new StringEncoder(CharsetUtil.UTF_8));
        pipeline.addLast("chat-client-myChatClientHandle",new MyChatClientHandle());
    }
}
