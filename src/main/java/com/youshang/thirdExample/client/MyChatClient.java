package com.youshang.thirdExample.client;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author youshang
 * @date 2021/08/08 22:04
 **/
public class MyChatClient {

    public static void main(String[] args) throws InterruptedException, IOException {
        EventLoopGroup eventExecutors = new NioEventLoopGroup();

        Bootstrap bootstrap = new Bootstrap();
        bootstrap.group(eventExecutors).channel(NioSocketChannel.class).handler(new MyChatClientInitializer());
        Channel channel = bootstrap.connect("127.0.0.1", 7788).sync().channel();

        while (true){
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            channel.writeAndFlush(bufferedReader.readLine()+"\r\n");
        }
        //channelFuture.channel().closeFuture().sync();
    }
}
