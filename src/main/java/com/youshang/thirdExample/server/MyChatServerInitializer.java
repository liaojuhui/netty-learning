package com.youshang.thirdExample.server;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.Delimiters;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.util.CharsetUtil;

/**
 *
 * @author youshang
 * @date 2021/08/08 21:56
 **/
public class MyChatServerInitializer extends ChannelInitializer<SocketChannel> {
    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline pipeline = ch.pipeline();

        pipeline.addLast("chat-server-delimiterBasedFrameDecoder",new DelimiterBasedFrameDecoder(4096, Delimiters.lineDelimiter()));
        pipeline.addLast("chat-server-stringDecoder",new StringDecoder(CharsetUtil.UTF_8));
        pipeline.addLast("chat-server-stringEncoder",new StringEncoder(CharsetUtil.UTF_8));
        pipeline.addLast("chat-server-myChatHandle",new MyChatServerHandle());
    }
}
