package com.youshang.thrift;

import com.youshang.thrift.generated.PersonService;
import com.youshang.thrift.service.impl.PersonServiceImpl;
import org.apache.thrift.TProcessorFactory;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.protocol.TSimpleJSONProtocol;
import org.apache.thrift.server.THsHaServer;
import org.apache.thrift.server.TNonblockingServer;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TThreadPoolServer;
import org.apache.thrift.transport.*;
import org.apache.thrift.transport.layered.TFramedTransport;

import java.io.InputStream;

/**
 *
 * @author youshang
 * @date 2021/08/15 20:07
 **/
public class MyThriftServer {

    public static void main(String[] args)  throws  Exception{
        TNonblockingServerSocket socket = new TNonblockingServerSocket(8899);
        THsHaServer.Args arg = new THsHaServer.Args(socket).minWorkerThreads(2).maxWorkerThreads(4);
        PersonService.Processor<PersonServiceImpl> processor = new PersonService.Processor<>(new PersonServiceImpl());
        //Thrift 传输格式
        arg.protocolFactory(new TCompactProtocol.Factory()); //压缩格式
        //arg.protocolFactory(new TSimpleJSONProtocol.Factory()); //提供JSON的只写协议，生成的文件很容易通过脚本语言解析
        //arg.protocolFactory(new TBinaryProtocol.Factory()); //二进制格式
        //arg.protocolFactory(new TSimpleJSONProtocol.Factory());//JSON 格式
        //Thrift 数据传输方式
        arg.transportFactory(new TFramedTransport.Factory()); //以frame为单位进行传输，非堵塞式的服务中使用
        //arg.transportFactory(new TSocket(null));//堵塞式socket
        //arg.transportFactory(new TFileTransport(null));//以文件传输形式进行传输
        //arg.transportFactory(new TMemoryTransport(null));//将内存用于I/O，Java实现时内部实际使用了简单的ByteArrayOutputStream
        //arg.transportFactory(new TZlibTransport.Factory());//使用zlib进行压缩，与其他传输方式联合使用，当前Java无法实现

        arg.processorFactory(new TProcessorFactory(processor));



        //Thrift支持的服务模型
        TServer server = new THsHaServer(arg);//THsha引入了线程池处理，其模型把读写任务放到线程池去处理，Half-sync/Half-async的处理模式，Half-async是在处理IO事件上（accept/read/write io），Half-sync用于handle对rpc的同步处理
        //TServer threadPoolServer = new TThreadPoolServer();//简单的单线程服务模型，常用于测试
        //TServer threadPoolServer = new TThreadPoolServer();//多线程服务模型，使用标准的堵塞式IO
        //TServer nonblockingServer = new TNonblockingServer();//多线程服务模型，使用非堵塞IO(需使用TFramedTransport数据传输方式)


        System.out.println("Thrift Server started!!");

        server.serve();
    }
}
