package com.youshang.thrift.service.impl;

import com.youshang.thrift.generated.DataException;
import com.youshang.thrift.generated.Person;
import com.youshang.thrift.generated.PersonService;
import org.apache.thrift.TException;

/**
 *
 * @author youshang
 * @date 2021/08/15 20:06
 **/
public class PersonServiceImpl implements PersonService.Iface {
    @Override
    public Person getPersonByUserName(String userName) throws DataException, TException {
        System.out.println("执行查询操作....."+ userName);
        Person person = new Person();
        person.setAge(20);
        short s = 89;
        person.setScore(s);
        person.setUsername(userName);
        person.setFlag(true);
        return person;
    }

    @Override
    public void savePerson(Person person) throws DataException, TException {
        System.out.println("执行save操作。。。。。");
        System.out.println(person.getAge());
        System.out.println(person.getUsername());
        System.out.println(person.getScore());
        System.out.println(person.isFlag());
    }
}
