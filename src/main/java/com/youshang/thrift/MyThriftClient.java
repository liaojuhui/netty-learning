package com.youshang.thrift;

import com.youshang.thrift.generated.Person;
import com.youshang.thrift.generated.PersonService;
import org.apache.thrift.TException;
import org.apache.thrift.TProcessor;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;
import org.apache.thrift.transport.layered.TFramedTransport;

/**
 *
 * @author youshang
 * @date 2021/08/15 20:07
 **/
public class MyThriftClient {

    public static void main(String[] args) throws TTransportException {
        TTransport tTransport = new TFramedTransport(new TSocket("localhost", 8899), 1000);
        TProtocol protocol = new TCompactProtocol(tTransport);
        PersonService.Client client = new PersonService.Client(protocol);

        tTransport.open();
        try {
            Person zhansan = client.getPersonByUserName("zhansan");
            client.savePerson(zhansan);

            System.out.println("操作成功。。。。。");
        } catch (TException e) {
            e.printStackTrace();
        }finally {
            tTransport.close();
        }

    }
}
