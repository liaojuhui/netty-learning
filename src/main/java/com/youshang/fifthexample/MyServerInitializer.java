package com.youshang.fifthexample;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import io.netty.handler.stream.ChunkedWriteHandler;

/**
 *
 * @author youshang
 * @date 2021/08/10 23:03
 **/
public class MyServerInitializer extends ChannelInitializer<SocketChannel> {
    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline pipeline = ch.pipeline();

        pipeline.addLast("myHttpServerCodec",new HttpServerCodec());
        pipeline.addLast("myChunkedWriteHandler",new ChunkedWriteHandler());
        pipeline.addLast("myHttpObjectAggregator",new HttpObjectAggregator(8192));
        pipeline.addLast("myWebSocketServerProtocolHandler",new WebSocketServerProtocolHandler("/ws"));
        pipeline.addLast("myMyServerHandle",new MyServerHandle());
    }
}
