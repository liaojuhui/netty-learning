package com.youshang.secondexample.client;

import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;

/**
 *
 * @author youshang
 * @date 2021/08/08 21:00
 **/
public class MyClientInitializer extends ChannelInitializer {
    @Override
    protected void initChannel(Channel ch) throws Exception {
        ChannelPipeline pipeline = ch.pipeline();

        pipeline.addLast("client-lengthFieldBasedFrameDecoder",new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE,0,4,0,4));
        pipeline.addLast("client-lengthFieldPrepender",new LengthFieldPrepender(4));
        pipeline.addLast("client-stringDecoder",new StringDecoder());
        pipeline.addLast("client-stringEncoder",new StringEncoder());
        pipeline.addLast("client-myClientHandle",new MyClientHandle());
    }
}
