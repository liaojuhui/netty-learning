package com.youshang.heartbeatexample.server;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.timeout.IdleStateHandler;

import java.util.concurrent.TimeUnit;

/**
 *
 * @author youshang
 * @date 2021/08/09 00:13
 **/
public class HeartbeatServerInitializer extends ChannelInitializer<SocketChannel> {
    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline pipeline = ch.pipeline();
        pipeline.addLast("server-idleStateHandler",new IdleStateHandler(5,7,10, TimeUnit.SECONDS));
        pipeline.addLast("server-myHeartbeatServerHandle",new MyHeartbeatServerHandle());
    }
}
