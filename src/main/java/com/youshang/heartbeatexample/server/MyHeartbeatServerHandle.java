package com.youshang.heartbeatexample.server;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.timeout.IdleStateEvent;

/**
 *
 * @author youshang
 * @date 2021/08/09 00:12
 **/
public class MyHeartbeatServerHandle extends ChannelInboundHandlerAdapter {

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        if (evt instanceof IdleStateEvent) {
            IdleStateEvent idleStateEvent = (IdleStateEvent) evt;
            String stateType = "";
            switch (idleStateEvent.state()){
                case READER_IDLE:
                    stateType = "读空闲";
                    break;
                case WRITER_IDLE:
                    stateType = "写空闲";
                    break;
                case ALL_IDLE:
                    stateType = "读写空闲";
                    break;
            }
            Channel channel = ctx.channel();
            System.out.println(channel.remoteAddress()+"  "+stateType + "\n");
            channel.writeAndFlush(channel.remoteAddress()+"  "+stateType + "\n");
            channel.close();
        }
    }
}
