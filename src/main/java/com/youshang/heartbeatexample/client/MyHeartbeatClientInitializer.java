package com.youshang.heartbeatexample.client;

import com.youshang.thirdExample.client.MyChatClientHandle;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.Delimiters;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.util.CharsetUtil;

import java.net.Socket;

/**
 *
 * @author youshang
 * @date 2021/08/09 00:23
 **/
public class MyHeartbeatClientInitializer extends ChannelInitializer<SocketChannel> {
    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline pipeline = ch.pipeline();
        pipeline.addLast("client-delimiterBasedFrameDecoder",new DelimiterBasedFrameDecoder(4096, Delimiters.lineDelimiter()));
        pipeline.addLast("client-stringDecoder",new StringDecoder(CharsetUtil.UTF_8));
        pipeline.addLast("client-stringEncoder",new StringEncoder(CharsetUtil.UTF_8));
        pipeline.addLast("client-myHeartbeatClientHandle",new MyHeartbeatClientHandle());
    }
}
