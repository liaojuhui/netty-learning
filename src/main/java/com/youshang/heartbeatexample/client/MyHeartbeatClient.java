package com.youshang.heartbeatexample.client;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author youshang
 * @date 2021/08/09 00:23
 **/
public class MyHeartbeatClient {

    public static void main(String[] args) throws InterruptedException, IOException {
        NioEventLoopGroup eventExecutors = new NioEventLoopGroup();

        Bootstrap bootstrap = new Bootstrap();
        bootstrap.group(eventExecutors).handler(new MyHeartbeatClientInitializer()).channel(NioSocketChannel.class);
        Channel channel = bootstrap.connect("localhost", 7788).sync().channel();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        while (true){
            channel.writeAndFlush(bufferedReader.readLine()+"\n");
        }
    }
}
