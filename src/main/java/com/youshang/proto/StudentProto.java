// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: Student.proto

package com.youshang.proto;

public final class StudentProto {
  private StudentProto() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_com_youshang_proto_MyRequest_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_com_youshang_proto_MyRequest_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_com_youshang_proto_MyResponse_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_com_youshang_proto_MyResponse_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_com_youshang_proto_StudentRequest_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_com_youshang_proto_StudentRequest_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_com_youshang_proto_StudentResponse_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_com_youshang_proto_StudentResponse_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_com_youshang_proto_StudentResponseList_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_com_youshang_proto_StudentResponseList_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_com_youshang_proto_StudentRequestList_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_com_youshang_proto_StudentRequestList_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n\rStudent.proto\022\022com.youshang.proto\"\035\n\tM" +
      "yRequest\022\020\n\010userName\030\001 \001(\t\"\036\n\nMyResponse" +
      "\022\020\n\010realName\030\001 \001(\t\"\035\n\016StudentRequest\022\013\n\003" +
      "age\030\001 \001(\005\",\n\017StudentResponse\022\014\n\004name\030\001 \001" +
      "(\t\022\013\n\003age\030\002 \001(\005\"S\n\023StudentResponseList\022<" +
      "\n\017studentResponse\030\001 \003(\0132#.com.youshang.p" +
      "roto.StudentResponse\"P\n\022StudentRequestLi" +
      "st\022:\n\016studentRequest\030\001 \003(\0132\".com.youshan" +
      "g.proto.StudentRequest2\223\003\n\016StudentServic" +
      "e\022X\n\025getRealNameByUsername\022\035.com.youshan",
      "g.proto.MyRequest\032\036.com.youshang.proto.M" +
      "yResponse\"\000\022_\n\020getStudentByAges\022\".com.yo" +
      "ushang.proto.StudentRequest\032#.com.yousha" +
      "ng.proto.StudentResponse\"\0000\001\022i\n\030getStude" +
      "ntsWrapperByAges\022\".com.youshang.proto.St" +
      "udentRequest\032\'.com.youshang.proto.Studen" +
      "tResponseList(\001\022[\n\006biTalk\022&.com.youshang" +
      ".proto.StudentRequestList\032\'.com.youshang" +
      ".proto.StudentResponseList(\001B$\n\022com.yous" +
      "hang.protoB\014StudentProtoP\001b\006proto3"
    };
    com.google.protobuf.Descriptors.FileDescriptor.InternalDescriptorAssigner assigner =
        new com.google.protobuf.Descriptors.FileDescriptor.    InternalDescriptorAssigner() {
          public com.google.protobuf.ExtensionRegistry assignDescriptors(
              com.google.protobuf.Descriptors.FileDescriptor root) {
            descriptor = root;
            return null;
          }
        };
    com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
        }, assigner);
    internal_static_com_youshang_proto_MyRequest_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_com_youshang_proto_MyRequest_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_com_youshang_proto_MyRequest_descriptor,
        new java.lang.String[] { "UserName", });
    internal_static_com_youshang_proto_MyResponse_descriptor =
      getDescriptor().getMessageTypes().get(1);
    internal_static_com_youshang_proto_MyResponse_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_com_youshang_proto_MyResponse_descriptor,
        new java.lang.String[] { "RealName", });
    internal_static_com_youshang_proto_StudentRequest_descriptor =
      getDescriptor().getMessageTypes().get(2);
    internal_static_com_youshang_proto_StudentRequest_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_com_youshang_proto_StudentRequest_descriptor,
        new java.lang.String[] { "Age", });
    internal_static_com_youshang_proto_StudentResponse_descriptor =
      getDescriptor().getMessageTypes().get(3);
    internal_static_com_youshang_proto_StudentResponse_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_com_youshang_proto_StudentResponse_descriptor,
        new java.lang.String[] { "Name", "Age", });
    internal_static_com_youshang_proto_StudentResponseList_descriptor =
      getDescriptor().getMessageTypes().get(4);
    internal_static_com_youshang_proto_StudentResponseList_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_com_youshang_proto_StudentResponseList_descriptor,
        new java.lang.String[] { "StudentResponse", });
    internal_static_com_youshang_proto_StudentRequestList_descriptor =
      getDescriptor().getMessageTypes().get(5);
    internal_static_com_youshang_proto_StudentRequestList_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_com_youshang_proto_StudentRequestList_descriptor,
        new java.lang.String[] { "StudentRequest", });
  }

  // @@protoc_insertion_point(outer_class_scope)
}
